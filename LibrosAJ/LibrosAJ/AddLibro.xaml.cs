﻿using LibrosAJ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LibrosAJ
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddLibro : ContentPage
    {
        private LibrosManager manager;
        private Libro libro;

        public AddLibro(LibrosManager manager, Libro libro = null)
        {   
            InitializeComponent();

            this.libro = libro;
            this.manager = manager;
        }
        async public void OnSaveLibro(object sender, EventArgs e)
        {
            await manager.Add(txtTitulo.Text, txtDetalle.Text, txtCategoria.Text, txtAutor.Text);
        }
        
    }
}