﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibrosAJ.Data
{
    public class Libro
    {
        public long Id { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
        public string Categoria { get; set; }
        public string Autor { get; set; }


    }
}
