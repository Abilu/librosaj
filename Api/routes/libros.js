'use strict';

var Libro = require('../models/Libro');
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', async function (req, res) {
    const libros = await Libro.findAll();
    res.json(libros);
});


router.post('/', async function (req, res) {
    let { Titulo, Detalle, Categoria, Autor } = req.body;
    let libro = await Libro.create({
        titulo: Titulo,
        detalle: Detalle,
        categoria: Categoria,
        autor: Autor
    });
    res.json(libro);
});

module.exports = router;
